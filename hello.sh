#!/bin/bash

#printf "$1 and totally $# arguments"

if [ $# -lt 1  ] 
then 
  printf "please provide a folder as argument..\n"
  exit 1
else 
  #ls -l $1 | awk -F ' ' '{ print $9 }' > result.txt
  ls -l $1 | awk -F ' ' ' { if (NF == 9) { print $9 }  }' > result.txt
  cat result.txt
fi


